import { Component, OnInit } from '@angular/core';
import { UserCreateComponent } from '../user-create/user-create.component';
import { MatDialog } from '@angular/material/dialog';
import { UserDeleteComponent } from '../user-delete/user-delete.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  users: any = [];

  constructor(public dialog: MatDialog, private router: Router) { }

  ngOnInit(): void {
  }

  openCreateDialog(): void {
    const dialogRef = this.dialog.open(UserCreateComponent, {
      width: '500px',
      data: {}
    });
    dialogRef.afterClosed().subscribe(user => {
      if (user != null)
        this.users.push(user);
    });
  }

  deleteUser(user, index): void {
    const dialogRef = this.dialog.open(UserDeleteComponent, {
      width: '500px',
      data: user
    });
    dialogRef.afterClosed().subscribe(res => {
      console.log(res);
      if(res){
        this.users.splice(index, 1);
      } 
    });
  }

  updateUser(user, index) {
    const dialogRef = this.dialog.open(UserCreateComponent, {
      width: '500px',
      data: user
    });
    dialogRef.afterClosed().subscribe(user => {
      if (user != null)
        this.users[index]=user;
    });
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

}
