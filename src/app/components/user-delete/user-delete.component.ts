import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.scss']
})
export class UserDeleteComponent implements OnInit {

  title: string;
  message: string;

  constructor(
    private _snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<UserDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.title = "Delete User";
    this.message = "Are you sure you want to delete " + this.data.email + "?";
  }

  onConfirm(): void {
    this.dialogRef.close(true);
    this._snackBar.open("User Deleted Successfully", 'OK', {
      duration: 5000
    });
  }

  onDismiss(): void {
    this.dialogRef.close(false);
  }

}
