import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DepartmentsService } from 'src/app/services/departments.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {
  form: FormGroup;
  isUpdate: boolean = false;
  btnName: string;
  submitted = false;
  departments: any = [];

  constructor(
    private departmentsService: DepartmentsService,
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<UserCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.getDepartments();
    if (Object.keys(this.data).length === 0) {
      this.isUpdate = false;
      this.btnName = "Create User";
    }
    else {
      this.isUpdate = true;
      this.btnName = "Update User";
    }
    this.form = this.fb.group({
      userName: [this.data.userName, Validators.compose([Validators.required, Validators.minLength(4), this.nameValidator()])],
      password: [this.data.password, Validators.compose([Validators.required, this.patternValidator()])],
      firstName: [this.data.firstName, Validators.compose([Validators.required, Validators.minLength(5), this.nameValidator()])],
      lastName: [this.data.lastName, Validators.compose([Validators.required, Validators.minLength(5), this.nameValidator()])],
      email: [this.data.email, [Validators.required, Validators.email]],
      phone: [this.data.phone, Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11), Validators.pattern(/^-?(|[0-9]\d*)?$/), this.phoneValidator()])],
      department: [this.data.department],
    });
  }

  patternValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const regex = new RegExp('^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9]+).{8,}$');
      const valid = regex.test(control.value);
      return valid ? null : { invalidPassword: true };
    };
  }

  nameValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const regex = new RegExp('^[a-zA-Z]');
      const valid = regex.test(control.value);
      return valid ? null : { invalidName: true };
    };
  }

  phoneValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const regex = new RegExp('^(010|012|015|011)');
      const valid = regex.test(control.value);
      return valid ? null : { invalidPhone: true };
    };
  }


  getDepartments(): void {
    this.departmentsService.getAllDepartments()
      .subscribe(
        departments => {
          this.departments = departments;
        },
        error => {
          console.log(error);
        });
  }

  createUser(): void {
    let modalMsg;
    const userData = {
      userName: this.form.controls.userName.value,
      firstName: this.form.controls.firstName.value,
      lastName: this.form.controls.lastName.value,
      email: this.form.controls.email.value,
      password: this.form.controls.password.value,
      phone: this.form.controls.phone.value,
      department: this.form.controls.department.value
    };
    this.submitted = true;
    if (this.form.valid) {
      this.dialogRef.close(userData);
      this.isUpdate ? modalMsg = "User Updated Successfully" : modalMsg = "User Created Successfully";
      this._snackBar.open(modalMsg, 'OK', {
        duration: 5000
      });
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
